package org.plugin.tsnsched.views;


import java.awt.Color;
import java.awt.Component;
import java.io.File;
import java.io.IOException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.eclipse.core.filebuffers.FileBuffers;
//import org.eclipse.core.filebuffers.ITextFileBuffer;
import org.eclipse.core.filebuffers.ITextFileBufferManager;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspace;
//import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.DialogSettings;
import org.eclipse.jface.dialogs.MessageDialog;
//import org.eclipse.jface.dialogs.PopupDialog;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.wb.swt.SWTResourceManager;
//import org.omnetpp.inifile.editor.model.ITimeout;
//import org.omnetpp.inifile.editor.model.InifileAnalyzer;
//import org.omnetpp.inifile.editor.model.InifileDocument;
//import org.omnetpp.inifile.editor.model.Timeout;
//import org.omnetpp.inifile.editor.text.InifileReconcileStrategy;
//import org.omnetpp.ned.core.NedResources;
import org.omnetpp.ned.core.NedResourcesPlugin;
import org.omnetpp.ned.model.INedElement;
import org.omnetpp.ned.model.ex.CompoundModuleElementEx;
import org.omnetpp.ned.model.ex.ConnectionElementEx;
//import org.omnetpp.ned.model.ex.NedFileElementEx;
import org.omnetpp.ned.model.ex.SubmoduleElementEx;
import org.omnetpp.ned.model.pojo.CommentElement;
import org.omnetpp.ned.model.pojo.ConnectionElement;
import org.omnetpp.ned.model.pojo.NedElementTags;
import org.omnetpp.ned.model.pojo.NedFileElement;
//import org.omnetpp.scave.engine.NodeType;
import org.plugin.model.FlowListEntry;
import org.plugin.model.MathOperations;
import org.plugin.model.SimplifiedInifileDocument;
//import org.plugin.model.SwitchPort;
import org.plugin.model.TSNNodeType;
import org.plugin.model.TSNSwitch;
import org.plugin.model.XMLNode;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
//import org.eclipse.swt.widgets.Menu;
//import org.eclipse.swt.widgets.MenuItem;

public class SetupView extends ViewPart {

	public static final String ID = "org.plugin.tsnsched.views.SetupView"; //$NON-NLS-1$
	private static final String defaultCycle = "400us";
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());

	private Composite scheduleView;
	private Composite fileSelect;
	private Composite container;
	private Composite scrolledGrid;
	private Composite compNodeList;
	private ScrolledComposite scrolledComposite;
	private ScrolledComposite scrolledComposite_1;
	
	private Text nedFileText;
	private Text textDefaultCycle;
	private Text schedFileText;
	private Text routFileText;	
	private Text iniFileText;
	/////
	private Text synthesisFileText;
	private Text flowFileText;

	////
	org.eclipse.swt.widgets.List listNodes;
	private final JFileChooser fileChooser = new JFileChooser();
	private final StackLayout layout = new StackLayout();


	///
	File last_dir, iniFile, nedFile, schedFile, routFile, synthesisFile, flowFile;
	///

	
	public static ArrayList<FlowListEntry> flowList = new ArrayList<FlowListEntry>();

	public static Map<String, ArrayList<FlowListEntry>> flowPeriods = new HashMap<>();
    
    
	ArrayList<String> hosts_sched,hosts_nonsched,switches;
	Map<String, TSNSwitch> switchMap;
	List<ConnectionElementEx> connections;
	Map<String,String> nodeToAddressMap, addressToNodeMap;
	Map<String,TSNNodeType>nodeToTypeMap;
	XMLNode networkNode;
	XMLNode xmlSched, xmlRout;



	public SetupView() {

	}

	@Override
	public void createPartControl(Composite parent) {
		//loadSettings();

		
		container = toolkit.createComposite(parent, SWT.NONE);
		toolkit.paintBordersFor(container);
		container.setLayout(layout);
		
		
		
		
		fileSelect = new Composite(container, SWT.NONE);
		toolkit.adapt(fileSelect);
		toolkit.paintBordersFor(fileSelect);
		
		//make three columns in the view
		GridLayout gl_fileSelect = new GridLayout(3, false);
		
		gl_fileSelect.verticalSpacing = 10;
		gl_fileSelect.horizontalSpacing = 10;
		gl_fileSelect.marginHeight = 10;
		
		fileSelect.setLayout(gl_fileSelect);
		//views[0] = fileSelect;
		
		
	///////////////////////////////////////////////////////////////////////////////////////////////	
		Label lblIniFile = new Label(fileSelect, SWT.NONE);
		lblIniFile.setAlignment(SWT.CENTER);
		toolkit.adapt(lblIniFile, true, true);
		lblIniFile.setText("ini file");
		// Check if an ini file is already loaded
		
		
		iniFileText = new Text(fileSelect, SWT.BORDER);
		iniFileText.setEnabled(false);
		GridData gd_iniFileText = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		gd_iniFileText.widthHint = 163;
		iniFileText.setLayoutData(gd_iniFileText);
		iniFileText.setEditable(false);
		
		toolkit.adapt(iniFileText, true, true);
		
		if(iniFile!=null) {
			iniFileText.setText(iniFile.getName());
		}

		Button btnLoadIni = new Button(fileSelect, SWT.NONE);
		btnLoadIni.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		btnLoadIni.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileNameExtensionFilter filter = new FileNameExtensionFilter(
					    "OMNeT++ Initialization files (.ini)", "ini");
				File temp = openFileDialog(filter,null);
				if(temp != null) {
					iniFile = temp;
					iniFileText.setText(iniFile.getName());
				}
			}
		});
		toolkit.adapt(btnLoadIni, true, true);
		btnLoadIni.setText("Load file");
	///////////////////////////////////////////////////////////////////////////////////////////////	
		Label lblNedFile = new Label(fileSelect, SWT.NONE);
		lblNedFile.setText("ned file");
		toolkit.adapt(lblNedFile, true, true);
		
		nedFileText = new Text(fileSelect, SWT.BORDER);
		nedFileText.setEnabled(false);
		nedFileText.setEditable(false);
		nedFileText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		
		toolkit.adapt(nedFileText, true, true);
		
		if(nedFile!=null) {
			nedFileText.setText(nedFile.getName());
		}
		
		Button btnLoadNed = new Button(fileSelect, SWT.NONE);
		btnLoadNed.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		btnLoadNed.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileNameExtensionFilter filter = new FileNameExtensionFilter(
					    "OMNeT++ Network Description files (.ned)", "ned");
				File temp = openFileDialog(filter,null);
				if(temp != null) {
					nedFile = temp;
					nedFileText.setText(nedFile.getName());
				}
			}
		});
		btnLoadNed.setText("Load file");
		toolkit.adapt(btnLoadNed, true, true);
		///////////////////////////////////////////////////////////////////////////////////////////////		
		Label lblFlowFile = new Label(fileSelect, SWT.NONE);
		lblFlowFile.setText("*Flow file");
		toolkit.adapt(lblFlowFile, true, true);

		flowFileText = new Text(fileSelect, SWT.BORDER);
		flowFileText.setEnabled(false);
		flowFileText.setEditable(false);
		flowFileText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		toolkit.adapt(flowFileText, true, true);

		if(synthesisFile!=null) {
			flowFileText.setText(flowFile.getName());
		}

		Button btnLoadFlow = new Button(fileSelect, SWT.NONE);
		btnLoadFlow.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileNameExtensionFilter filter = new FileNameExtensionFilter(
						"*Flow files (.csv)", "csv");
				File temp = openFileDialog(filter,null);
				if(temp != null) {
					flowFile = temp;
					flowFileText.setText(flowFile.getName());
				}	
			}
		});
		btnLoadFlow.setText("Load file");
		toolkit.adapt(btnLoadFlow, true, true);
		///////////////////////////////////////////////////////////////////////////////////////////////	
		Label lblXmlScheduleFile = new Label(fileSelect, SWT.NONE);
		lblXmlScheduleFile.setText("XML schedule file");
		toolkit.adapt(lblXmlScheduleFile, true, true);
		
		schedFileText = new Text(fileSelect, SWT.BORDER);
		schedFileText.setEnabled(false);
		schedFileText.setEditable(false);
		schedFileText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		
		toolkit.adapt(schedFileText, true, true);
		
		if(schedFile!=null) {
			schedFileText.setText(schedFile.getName());
		}
		
		Button btnLoadSched = new Button(fileSelect, SWT.NONE);
		btnLoadSched.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileNameExtensionFilter filter = new FileNameExtensionFilter(
					    "NeSTiNG Schedule files (.xml)", "xml");
				File temp = openFileDialog(filter,null);
				if(temp != null) {
					schedFile = temp;
					schedFileText.setText(schedFile.getName());
				}
			}
		});
		btnLoadSched.setText("Load file");
		toolkit.adapt(btnLoadSched, true, true);

	///////////////////////////////////////////////////////////////////////////////////////////////	
		Label lblXmlRoutingFile = new Label(fileSelect, SWT.NONE);
		lblXmlRoutingFile.setText("XML routing file");
		toolkit.adapt(lblXmlRoutingFile, true, true);
		
		routFileText = new Text(fileSelect, SWT.BORDER);
		routFileText.setEnabled(false);
		routFileText.setEditable(false);
		routFileText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		
		toolkit.adapt(routFileText, true, true);
		
		if(routFile!=null) {
			routFileText.setText(routFile.getName());
			}
		
		Button btnLoadRout = new Button(fileSelect, SWT.NONE);
		btnLoadRout.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileNameExtensionFilter filter = new FileNameExtensionFilter(
					    "NeSTiNG Routing files (.xml)", "xml");
				File temp = openFileDialog(filter,null);
				if(temp != null) {
					routFile = temp;
					routFileText.setText(routFile.getName());
				}
			}
		});
		btnLoadRout.setText("Load file");
		toolkit.adapt(btnLoadRout, true, true);
		

		///////////////////////////////////////////////////////////////////////////////////////////////	
		Label lblGateSynthesisFile = new Label(fileSelect, SWT.NONE);
		lblGateSynthesisFile.setText("*Synth. file");
		Color c;
		toolkit.adapt(lblGateSynthesisFile, true, true);
		
		synthesisFileText = new Text(fileSelect, SWT.BORDER);
		synthesisFileText.setEnabled(false);
		synthesisFileText.setEditable(false);
		synthesisFileText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		
		toolkit.adapt(synthesisFileText, true, true);
		
		if(synthesisFile!=null) {
			synthesisFileText.setText(synthesisFile.getName());
			}
		
		Button btnLoadSynthesis = new Button(fileSelect, SWT.NONE);
		btnLoadSynthesis.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileNameExtensionFilter filter = new FileNameExtensionFilter(
					    "*Synthesis files (.csv)", "csv");
				File temp = openFileDialog(filter,null);
				if(temp != null) {
					synthesisFile = temp;
					synthesisFileText.setText(synthesisFile.getName());
				}
			}
		});
		btnLoadSynthesis.setText("Load file");
		toolkit.adapt(btnLoadSynthesis, true, true);
///////////////////////////////////////////////////////////////////////////////////////////////		


		new Label(fileSelect, SWT.NONE);
	
		Label label_5_1 = new Label(fileSelect, SWT.NONE);
		toolkit.adapt(label_5_1, true, true);
		new Label(fileSelect, SWT.NONE);
		new Label(fileSelect, SWT.NONE);
		
		Label label_5_2 = new Label(fileSelect, SWT.NONE);
		toolkit.adapt(label_5_2, true, true);
		
		Button btnNext = new Button(fileSelect, SWT.NONE);
		btnNext.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				/////////////
				if(iniFile == null || nedFile == null || schedFile == null || routFile == null) {
					Shell shell = parent.getShell();
					MessageDialog.openWarning(shell, "Missing files", "One or more files are missing");
				}
				else {
					// Load the selected files and switch to the next view
					if(loadFiles()) {
						switchCurrentView(1);
					}

				}
			}
		});
		btnNext.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		toolkit.adapt(btnNext, true, true);
		btnNext.setText("Start");
		toolkit.adapt(btnNext, true, true);

	///////////////////////////////////////////////////////////////////////////////////////////////	
		
		Button btnAuto = new Button(fileSelect, SWT.NONE);
		btnAuto.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				//Check if everything is selected
				/////////////
				if(synthesisFile != null || flowFile != null){
				
					Shell shell = parent.getShell();
					MessageDialog.openWarning(shell, "Auto configuration...", "Auto gate configuration received...");
				} 
				
				/////////////
				if(synthesisFile == null || flowFile == null || iniFile == null || nedFile == null || schedFile == null || routFile == null) {
					Shell shell = parent.getShell();
					MessageDialog.openWarning(shell, "Missing files", "One or more files are missing!");
				}
				else {
					
					//Start Auto configuration
					//Prompt saved files
					if(loadAutoFiles()) {
						//switchCurrentView(1);
	/////PROCESS STARTS

						pars_XML_sched();
						
						parseFlowSynthesis(flowFile);
				        parseOffset(synthesisFile);

						generateEntryHashed();
						ArrayList<String []> gate = parseGateSynthesis(synthesisFile);
				        addSwitchInformation2(gate);
						//SetupNodeListWindow();
						
						Shell shell = parent.getShell();
						MessageDialog.openWarning(shell, "Auto configuration...50%", "Parsed config. files successfully!");
						GenerateXMLFiles();
						MessageDialog.openWarning(shell, "Auto configuration...100%", "Please, do not press auto configure again!\n Generated configs can be edited by pressing start.");

	/////PROCESS FINISHED
					}
				}
			}
		});
		btnAuto.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		toolkit.adapt(btnAuto, true, true);
		btnAuto.setText("*Auto configure");
	

	///////////////////////////////////////////////////////////////////////////////////////////////	
		scheduleView = new Composite(container, SWT.NONE);
		scheduleView.setBackground(SWTResourceManager.getColor(SWT.COLOR_TITLE_BACKGROUND_GRADIENT));
		toolkit.adapt(scheduleView);
		toolkit.paintBordersFor(scheduleView);
		scheduleView.setLayout(new FormLayout());
		
		Button btnSave = new Button(scheduleView, SWT.FLAT);
		btnSave.addSelectionListener(new SelectionAdapter(){
			
		@Override
		public void widgetSelected(SelectionEvent e) {
			try {
				GenerateXMLFiles();
				}
			catch(Exception er) 
			{
				MessageDialog.openError(scheduleView.getShell(), "Error", "An error occured during configuration saved.\n" + er);
			}
		}});
		btnSave.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.BOLD));
		btnSave.setForeground(SWTResourceManager.getColor(SWT.COLOR_MAGENTA));
		FormData fd_btnSave = new FormData();
		fd_btnSave.bottom = new FormAttachment(100, -10);
		fd_btnSave.right = new FormAttachment(100, -10);
		btnSave.setLayoutData(fd_btnSave);
		toolkit.adapt(btnSave, true, true);
		btnSave.setText("Generate configuration");
		
		Button btnNewButton = new Button(scheduleView, SWT.NONE);
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				addEntry();
			}
		});
		FormData fd_btnNewButton = new FormData();
		fd_btnNewButton.left = new FormAttachment(btnSave, 0, SWT.LEFT);
		btnNewButton.setLayoutData(fd_btnNewButton);
		toolkit.adapt(btnNewButton, true, true);
		btnNewButton.setText("Add Flow");
		
		scrolledComposite = new ScrolledComposite(scheduleView, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		fd_btnNewButton.bottom = new FormAttachment(scrolledComposite, -6);
		FormData fd_scrolledComposite = new FormData();
		fd_scrolledComposite.bottom = new FormAttachment(btnSave, -6);
		fd_scrolledComposite.right = new FormAttachment(btnSave, -126, SWT.RIGHT);
		fd_scrolledComposite.top = new FormAttachment(0, 41);
		fd_scrolledComposite.left = new FormAttachment(0, 10);
		scrolledComposite.setLayoutData(fd_scrolledComposite);
		toolkit.adapt(scrolledComposite);
		toolkit.paintBordersFor(scrolledComposite);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);	
		
		Button btnBack = new Button(scheduleView, SWT.FLAT);
		btnBack.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				switchCurrentView(0);
			}
		});
		btnBack.setText("Back");
		btnBack.setForeground(SWTResourceManager.getColor(SWT.COLOR_MAGENTA));
		btnBack.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.BOLD));
		FormData fd_btnBack = new FormData();
		fd_btnBack.left = new FormAttachment(btnSave, -89, SWT.LEFT);
		fd_btnBack.bottom = new FormAttachment(btnSave, 0, SWT.BOTTOM);
		fd_btnBack.right = new FormAttachment(btnSave, -6);
		btnBack.setLayoutData(fd_btnBack);
		toolkit.adapt(btnBack, true, true);
		
		//FlowListEntry entry = new FlowListEntry(scrolledGrid);
		//entry.adapt(toolkit);
		
		Label lblListOfNodes = new Label(scheduleView, SWT.NONE);
		FormData fd_lblListOfNodes = new FormData();
		fd_lblListOfNodes.bottom = new FormAttachment(btnNewButton, 0, SWT.BOTTOM);
		lblListOfNodes.setLayoutData(fd_lblListOfNodes);
		toolkit.adapt(lblListOfNodes, true, true);
		lblListOfNodes.setText("List of nodes");
		
		scrolledComposite_1 = new ScrolledComposite(scheduleView, SWT.BORDER | SWT.V_SCROLL);
		fd_lblListOfNodes.top = new FormAttachment(scrolledComposite_1, -21, SWT.TOP);
		fd_lblListOfNodes.left = new FormAttachment(scrolledComposite_1, 0, SWT.LEFT);
		scrolledComposite_1.setMinWidth(190);
		scrolledComposite_1.setMinHeight(90);
		FormData fd_scrolledComposite_1 = new FormData();
		fd_scrolledComposite_1.top = new FormAttachment(scrolledComposite, 0, SWT.TOP);
		fd_scrolledComposite_1.right = new FormAttachment(100, -10);
		
		scrolledGrid = new Composite(scrolledComposite, SWT.BORDER);
		toolkit.adapt(scrolledGrid);
		toolkit.paintBordersFor(scrolledGrid);
		scrolledGrid.setLayout(new GridLayout(1, false));
		scrolledComposite.setContent(scrolledGrid);
		scrolledComposite.setMinSize(scrolledGrid.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		//scrolledComposite.setMinSize(scrolledGrid.computeSize(scrolledComposite_1.getSize().x, SWT.DEFAULT));

		
		
		scrolledComposite_1.setLayoutData(fd_scrolledComposite_1);
		toolkit.adapt(scrolledComposite_1);
		toolkit.paintBordersFor(scrolledComposite_1);
		scrolledComposite_1.setExpandHorizontal(true);
		scrolledComposite_1.setExpandVertical(true);
		
		compNodeList = new Composite(scrolledComposite_1, SWT.NONE);
		compNodeList.setLayout(new GridLayout(1, false));
		toolkit.adapt(compNodeList);
		toolkit.paintBordersFor(compNodeList);
		
		listNodes = new org.eclipse.swt.widgets.List(compNodeList, SWT.NONE);
		listNodes.setItems(new String[] {"Workstation 1", "Workstation 2", "Workstation 3", "Workstation 1", "Workstation 2", "Workstation 3", "Workstation 1", "Workstation 2", "Workstation 3", "Workstation 1", "Workstation 2", "Workstation 3"});
		toolkit.adapt(listNodes, true, true);
		scrolledComposite_1.setContent(compNodeList);
		scrolledComposite_1.setMinSize(compNodeList.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		
		// Show information about node when double clicked
		listNodes.addListener(SWT.MouseDoubleClick, new Listener() {
			
			@Override
			public void handleEvent(Event event) {
				String selectedNode = listNodes.getSelection()[0];
				openInformationDialog(selectedNode,null);
				
			}
		});
		
		Label lblTsnFlows = new Label(scheduleView, SWT.NONE);
		FormData fd_lblTsnFlows = new FormData();
		fd_lblTsnFlows.bottom = new FormAttachment(scrolledComposite, -6);
		fd_lblTsnFlows.left = new FormAttachment(0, 10);
		lblTsnFlows.setLayoutData(fd_lblTsnFlows);
		toolkit.adapt(lblTsnFlows, true, true);
		lblTsnFlows.setText("TSN flows");
		
		Label lblDefaultCycle = new Label(scheduleView, SWT.NONE);
		FormData fd_lblDefaultCycle = new FormData();
		fd_lblDefaultCycle.top = new FormAttachment(scrolledComposite_1, 6);
		fd_lblDefaultCycle.left = new FormAttachment(lblListOfNodes, 0, SWT.LEFT);
		lblDefaultCycle.setLayoutData(fd_lblDefaultCycle);
		toolkit.adapt(lblDefaultCycle, true, true);
		lblDefaultCycle.setText("Default cycle");
		
		textDefaultCycle = new Text(scheduleView, SWT.BORDER);
		textDefaultCycle.setEditable(false);
		FormData fd_textDefaultCycle = new FormData();
		fd_textDefaultCycle.right = new FormAttachment(btnSave, 0, SWT.RIGHT);
		fd_textDefaultCycle.top = new FormAttachment(lblDefaultCycle, 6);
		fd_textDefaultCycle.left = new FormAttachment(lblListOfNodes, 0, SWT.LEFT);
		textDefaultCycle.setLayoutData(fd_textDefaultCycle);
		toolkit.adapt(textDefaultCycle, true, true);
		
		Button btnUpdateCycle = new Button(scheduleView, SWT.NONE);
		btnUpdateCycle.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(flowList.size()>0){
					//double DefaultCycle = 1;
					int i = 0;
					double cycles[] = new double[flowList.size()];
					for (FlowListEntry flow : flowList) {
						cycles[i] = flow.getCycle();
						i++;
					}
					textDefaultCycle.setText(Integer.toString((int)MathOperations.lcm(cycles)) + "us");
				}
				
			}
		});
		FormData fd_btnUpdateCycle = new FormData();
		fd_btnUpdateCycle.right = new FormAttachment(btnSave, 0, SWT.RIGHT);
		fd_btnUpdateCycle.top = new FormAttachment(textDefaultCycle, 6);
		fd_btnUpdateCycle.left = new FormAttachment(scrolledComposite, 0);
		btnUpdateCycle.setLayoutData(fd_btnUpdateCycle);
		toolkit.adapt(btnUpdateCycle, true, true);
		btnUpdateCycle.setText("Update cycle");

		
		switchCurrentView(0);
		createActions();
		initializeToolBar();
		initializeMenu();
		
	}

	protected void GenerateXMLFiles() {
		// Get the data
		XMLNode sched = new XMLNode("schedule");
		XMLNode rout = new XMLNode("filteringDatabases");
		XMLNode defaultcycle = new XMLNode("defaultcycle");
		defaultcycle.add(new XMLNode(textDefaultCycle.getText()));
		
		sched.add(defaultcycle);
		
		
		// Get all flows
		ArrayList<XMLNode> hosts = new ArrayList<XMLNode>();
		
		// Map with each rout entry (switch, port) -> address
		HashMap<String, HashMap<String,String>> switchRoutMap = new HashMap<String, HashMap<String,String>>();

		int flowID = 1;
		Collections.sort(flowList, Comparator.comparing(FlowListEntry::getStart));
	
		for ( FlowListEntry flow : flowList) {

			Boolean dup=false;
			
			// setup routing
			ArrayList<String> path = flow.GetPath();
			int i=0;
			String current, next, port, target;
			target = path.get(path.size()-1); // Get last visited node

			target = nodeToAddressMap.get(target); //Convert to address

			while(i<path.size()-1) {
				current = path.get(i);
				next = path.get(i+1);

				// Create a hashmap if on doesn't exist
				if(!switchRoutMap.containsKey(current)) {
					switchRoutMap.put(current, new HashMap<String, String>());
				}
				port = networkNode.getByName(current).getByName(next).getAttributeByName("port");
				switchRoutMap.get(current).put(target,port);
				i++;
			}
			
			
			//If host already exist, place the entry with the existing host
			for (XMLNode xmlNode : hosts) {

				if(xmlNode.getAttributeByName("name").equals(flow.getSource())) {
					XMLNode entry = flow.toXMLEntry();
					//Replace dest address
					String address = entry.getByName("dest").get(0).toString();
					address = nodeToAddressMap.get(address);
					entry.getByName("dest").get(0).string = address;
					XMLNode nodeFlowId = new XMLNode("flowId");
					nodeFlowId.add(new XMLNode(Integer.toString(flowID)));
					entry.add(nodeFlowId); 
					flowID = flowID + 1;
					xmlNode.add(entry);
					dup = true;
					break;
				}
			}
			if(dup)
				continue;
			
			// Give each flow an unique flow id
			XMLNode node = flow.toXMLHost();
			XMLNode entry = flow.toXMLEntry();
			XMLNode nodeFlowId = new XMLNode("flowId");
			nodeFlowId.add(new XMLNode(Integer.toString(flowID)));
			entry.add(nodeFlowId); 
			flowID = flowID + 1;
			
			
			//Replace dest address
			String address = entry.getByName("dest").get(0).toString();
			address = nodeToAddressMap.get(address);
			entry.getByName("dest").get(0).string = address;
			node.add(entry);
			hosts.add(node);
		}
		for (XMLNode xmlNode : hosts) {

			sched.add(xmlNode);
		}
		
		
		//Add switches gate schedule
		for(String str_switch : switches){

			TSNSwitch tsnSwitch = switchMap.get(str_switch);

			XMLNode xmlSwitch =  tsnSwitch.getXMLNode();
			
	        
			if(xmlSwitch.getByName("port") != null) {
		    
		        sched.add(xmlSwitch);

			}
		}
		
		//Add routing table
		for(Entry<String, HashMap<String, String>> element : switchRoutMap.entrySet()) {
			XMLNode switchEntry = new XMLNode("filteringDatabase");
			switchEntry.addAttributes("id",element.getKey());
			XMLNode Static = new XMLNode("static");
			XMLNode Forward = new XMLNode("forward");
			
			for(Entry<String, String> entry : element.getValue().entrySet()) {
				XMLNode xmlEntry = new XMLNode("individualAddress");
				xmlEntry.addAttributes("macAddress", entry.getKey());
				xmlEntry.addAttributes("port", entry.getValue());
				Forward.add(xmlEntry);
			}
			Static.add(Forward);
			switchEntry.add(Static);
			rout.add(switchEntry);
		}
		/*
		<filteringDatabases>
			<filteringDatabase id="switchA">
			    <static>
			        <forward>
			        	<!-- Forward packets addressed to robotArm to switchB -->
			        	<individualAddress macAddress="00-00-00-00-00-04" port="3" />
			        	<!-- Forward packets addressed to backupServer to switchB -->
			        	<individualAddress macAddress="00-00-00-00-00-05" port="3" />
			        </forward>
			    </static>
			</filteringDatabase>
		</filteringDatabases>
		*/
		
		//Save to the file location
		sched.saveToXML(schedFile);
		rout.saveToXML(routFile);
		MessageDialog.openInformation(listNodes.getShell(), "Info", "Saved files sucessfully");
	}
	public void openInformationDialog(String node, Dialog dialog) {
		if(dialog != null) {
			// Close any previus dialog
			dialog.close();
		}
		Shell shell = listNodes.getShell();
		NodeInformationPopup informationPopup = new NodeInformationPopup(shell);
		informationPopup.setNodeName(node);
		informationPopup.setConnections(networkNode.getByName(node).children);
		informationPopup.setCallback(SetupView.this);
		informationPopup.open();
	}
	private File openFileDialog(FileNameExtensionFilter filter, Component parent) {
		try {
			fileChooser.setFileFilter(filter);
			fileChooser.setCurrentDirectory(last_dir);
			int returnVal = fileChooser.showOpenDialog(parent);
			if(returnVal == JFileChooser.APPROVE_OPTION) {
			   last_dir = fileChooser.getSelectedFile().getParentFile();
			   return fileChooser.getSelectedFile();
			}
		}catch(Exception ex) {}
		return null;
	}
	private void switchCurrentView(int i) {
		
		Control[] c = container.getChildren();
		layout.topControl = c[i];
		container.layout();

	}
	public void dispose() {
		saveSettings();
		toolkit.dispose();
		super.dispose();
	}
	
	//add Entry menu by window
	public void addEntry() {
		if(hosts_sched.size()==0) {
			MessageDialog.openError(scheduleView.getShell(), "Error", "No available host exists on the network");
			return;
		}
		ElementListSelectionDialog dialog = new ElementListSelectionDialog(scheduleView.getShell(), new LabelProvider());
		dialog.setElements(hosts_sched.toArray());
		dialog.setTitle("Pick the source host");
		dialog.setDialogHelpAvailable(false);
		
		
		// user pressed cancel
		if (dialog.open() != Window.OK) {
			return;
		}
		Object[] result = dialog.getResult();
		addEntry((String)result[0],"Default","0",7,100,"200",new ArrayList<String>());
		

	}//Default 
	//add Entry by parser
	public void addEntry(String source, String destination, String start, int queue, int size, String period_value,ArrayList<String> path) {
		
		FlowListEntry entry = new FlowListEntry(scrolledGrid, source, destination, start, queue, size, period_value, path, "0");
		entry.btnAddNode.addSelectionListener(new SelectionAdapter() {
			@Override
			
			
			public void widgetSelected(SelectionEvent e) {
				String lastNode = entry.GetLatestPathNode();
				
				// Only expand if the current last node is a switch or this is the first node
				if(nodeToTypeMap.get(lastNode)==TSNNodeType.SWITCH || entry.GetPath().size()==0) {
					
					ArrayList<String>potentialConnection = new ArrayList<String>(); //Convert to strings list 
					for(XMLNode node : networkNode.getByName(lastNode).children){
						potentialConnection.add(node.toString());
					}
					ArrayList<String> visited = entry.GetPath();
					// Remove previusly visited nodes
					potentialConnection.removeAll(visited);
					potentialConnection.remove(source);
					
					if(potentialConnection.size()==0) {
						// No available connections for this node
						MessageDialog.openError(scheduleView.getShell(), "Error", "No available connections exists for this node");
					}
					else if(potentialConnection.size()==1) {
						// Only one node to pick. Expand this automatically
						entry.AddPathNode(potentialConnection.get(0));
					}
					else {
						// Present dialog
						ElementListSelectionDialog dialog = new ElementListSelectionDialog(scheduleView.getShell(), new LabelProvider());
						dialog.setElements(potentialConnection.toArray());
						dialog.setTitle("Pick the next node");
						dialog.setDialogHelpAvailable(false);
						if (dialog.open() != Window.OK) {
							return; // user pressed cancel
						}
						Object[] result = dialog.getResult();
						entry.AddPathNode((String)result[0]);
					}
					
					scrolledGrid.layout();
					// Disable button if it can't expand anymore
					
					if(nodeToTypeMap.get(entry.GetLatestPathNode())!=TSNNodeType.SWITCH) {
						entry.btnAddNode.setEnabled(false);
						}
				}
				
			}
		});
		// Disable button if it has more then one node and can't expand anymore
		if(nodeToTypeMap.get(entry.GetLatestPathNode())!=TSNNodeType.SWITCH &&  entry.GetPath().size()!=0) {
			
			entry.btnAddNode.setEnabled(false);
			
		}

		scrolledGrid.setSize(scrolledGrid.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		scrolledComposite.setContent(scrolledGrid);
		scrolledComposite.setMinSize(scrolledGrid.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		entry.btnRemove.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
		
				flowList.remove(entry);
				
				entry.composite.dispose();
				
				scrolledComposite.setContent(scrolledGrid);
				
				scrolledComposite.setMinSize(scrolledGrid.computeSize(SWT.DEFAULT, SWT.DEFAULT));
			}
		});

		flowList.add(entry);

	}
	
	
	//add Entry by parser
	public void addEntryAuto(String index, String source, String destination, String start, int queue, int size, String period_value,ArrayList<String> path) {
		
		FlowListEntry entry = new FlowListEntry(scrolledGrid, source, destination, start, queue, size, period_value, path, index);
		
		 if (flowPeriods.get(source) == null) {
			    
			 	ArrayList<FlowListEntry> entryPeriods = new ArrayList<FlowListEntry>();
				entryPeriods.add(entry);
			    System.out.println(entry.getSource() +":"+ entry.getCycleStr());

				flowPeriods.put(source, entryPeriods);


		 } else {
			 
			 	ArrayList<FlowListEntry> entryPeriods = flowPeriods.get(source);
				entryPeriods.add(entry);
			    System.out.println(entry.getSource() +":"+ entry.getCycleStr());

				flowPeriods.put(source, entryPeriods);

		}
		//flowList.add(entry);

		
   

	}
	public void generateEntryHashed() {

	    System.out.println(flowPeriods.size());
	
		for(Map.Entry<String, ArrayList<FlowListEntry>> Iter : flowPeriods.entrySet()) {
			String key = Iter.getKey();
			ArrayList<FlowListEntry> value = Iter.getValue();
				    
			
			int counter = 0;
			//int max = value.size();

			Double [] sourcePeriods = new Double [100];
			for(FlowListEntry flowEntry : value) {
				
				double period = flowEntry.getCycle();

				sourcePeriods[counter] = period;

				
				counter++;
			}
			
			double lcmOfSrc = MathOperations.lcm(sourcePeriods);

			//System.out.println("Hyperperiod:" +lcmOfSrc);

			
			//sort flowEntry by 

			for(FlowListEntry flowEntry : value) { 
				double prd = flowEntry.getCycle();
				double offset = flowEntry.getStart();
				double activation = offset;
				System.out.println("ID: " + flowEntry.getID() +"|" + flowEntry.getSource() +"|" + flowEntry.getDestination() +"| offset: " + flowEntry.getStart() );
				int instanceCounter = 0;

				for(double iter = 0; iter < lcmOfSrc; iter += prd) {
					
					instanceCounter++;
					activation = iter + offset;
					System.out.println("StreamCounter"+instanceCounter+":"+ flowEntry.getSource() +":"+ lcmOfSrc +":"+ activation);
					
					
					FlowListEntry entry = new FlowListEntry(scrolledGrid, flowEntry.getSource(), flowEntry.getDestination(), Double.toString(activation), flowEntry.getQueue(), flowEntry.getSize(), Double.toString(lcmOfSrc), flowEntry.GetPath(), flowEntry.getID());
					
					flowList.add(entry);

					} 
				}
		
		}
    } 
		
	
	
	/**
	 * Create the actions.
	 */
	private void createActions() {
		// Create the actions
	}

	private void clearAllFlows() {
		flowList.clear();
		flowPeriods.clear();
		
		
		Control[] children = scrolledGrid.getChildren();
		for (Control control : children) {
			control.dispose();
		}
		
	}
	/**
	 * Initialize the toolbar.
	 */
	private void initializeToolBar() {
		IToolBarManager tbm = getViewSite().getActionBars().getToolBarManager();
	}

	/**
	 * Initialize the menu.
	 */
	private void initializeMenu() {
		IMenuManager manager = getViewSite().getActionBars().getMenuManager();
	}
	
	private void loadSettings() {
		// Load previously files
		IPath path = org.plugin.tsnsched.Activator.getDefault().getStateLocation();
		String filename = path.append("settings.txt").toOSString();
		DialogSettings settings = new DialogSettings("Top");
		try {
			settings.load(filename);
			
			if(settings.get("last_dir")!=null) {
				this.last_dir = new File(settings.get("last_dir"));
			}
			if(settings.get("iniFile")!=null) {
				this.iniFile = new File(settings.get("iniFile"));
			}
			if(settings.get("nedFile")!=null) {
				this.nedFile = new File(settings.get("nedFile"));
			}
			if(settings.get("schedFile")!=null) {
				this.schedFile = new File(settings.get("schedFile"));
			}
			if(settings.get("routFile")!=null) {
				this.routFile = new File(settings.get("routFile"));
			}
			/////////////////
			//if(settings.get("synthesisFile")!=null) {
				//this.synthesisFile = new File(settings.get("synthesisFile"));
			//}
			///////////////////
			
			
		} catch (IOException e1) {
			e1.printStackTrace();
			
		}
	}
	private void saveSettings() {
		
		IPath path = org.plugin.tsnsched.Activator.getDefault().getStateLocation();
		String filename = path.append("settings.txt").toOSString();
		DialogSettings settings = new DialogSettings("Top");
		try {
			if(last_dir!=null && last_dir.exists()) {
				settings.put("last_dir", last_dir.toString());
				}
			if(iniFile!=null && iniFile.exists()) {
				settings.put("iniFile", iniFile.toString());
				}
			if(nedFile!=null && nedFile.exists()) {
				settings.put("nedFile", nedFile.toString());
				}
			if(schedFile!=null && schedFile.exists()) {
				settings.put("schedFile", schedFile.toString());
				}
			if(routFile!=null && routFile.exists()) {
				settings.put("routFile", routFile.toString());
				}
			settings.save(filename);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	private Boolean loadFiles() {
		
		if(read_INI_NED_Files()==false) {
			//Error when reading ini or ned
			MessageDialog.openError(scheduleView.getShell(), "Error", "Could not read ini/ned. Are the files outside the workspace?");
			return false;
		}
		clearAllFlows();
		if((xmlSched = read_XML_to_XMLNodefile(schedFile))==null) {
			return false;
			} //Try reading schedule
		if((xmlRout = read_XML_to_XMLNodefile(routFile))==null) {
			return false;
			} //Try reading rout

		pars_XML_sched();
		if(addSwitchInformation()==false){
			MessageDialog.openError(scheduleView.getShell(), "Error", "Could not parse switch information from xml");
			return false;
		}
		SetupNodeListWindow();


		return true;
	}
	
	private Boolean loadAutoFiles() {
		
		//Error when reading ini or ned
		if(read_INI_NED_Files()==false) 
		{	
			MessageDialog.openError(scheduleView.getShell(), "Error", "Could not read ini/ned. Are the files outside the workspace?");
			return false;
		}
		
		clearAllFlows();
		
		 //Try reading schedule
		if((xmlSched = read_XML_to_XMLNodefile(schedFile))==null) 
		{
			return false;
		}
		//Try reading rout
		if((xmlRout = read_XML_to_XMLNodefile(routFile))==null) 
		{
			return false;
		}

		
		//Try reading csv flow
		if(CSV_is_empty(flowFile)) 
		{
			MessageDialog.openError(scheduleView.getShell(), "Error", "CSV is empty");
			return false;
		}
		//Try reading csv synthesis
		if(CSV_is_empty(synthesisFile)) 
		{
			MessageDialog.openError(scheduleView.getShell(), "Error", "CSV is empty");
			return false;
		}
		
		if(addSwitchInformation()==false)
		{
			MessageDialog.openError(scheduleView.getShell(), "Error", "Could not parse switch information from xml");
			return false;
		}

		return true;
	}
	
	void SetupNodeListWindow(){
		List<String> nodeString = new ArrayList<String>();
		for (XMLNode node : networkNode.children) {
			nodeString.add(node.toString());
		}
		String[] stringArray = Arrays.copyOf(nodeString.toArray(), nodeString.size(), String[].class);
		listNodes.setItems(stringArray);
		compNodeList.setSize(compNodeList.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		compNodeList.layout(true,true);
	}
	public Boolean pars_XML_sched() {
		try {
			XMLNode defaultcycleNode = xmlSched.getByName("defaultcycle");
			if(defaultcycleNode != null) {
				textDefaultCycle.setText(defaultcycleNode.get(0).toString());
			}
			else {
				textDefaultCycle.setText(defaultCycle);
			}
			ArrayList<XMLNode> hostList = xmlSched.getAllWithName("host");
			
			// Parse each node with type host
			for (XMLNode xmlNode : hostList) {
				ArrayList<XMLNode> entrylist = xmlNode.getAllWithName("entry");
				
			
				// Add each entry in the host
				for (XMLNode entry : entrylist) {
					// Get name of node source and destination
					String source = xmlNode.attributesvalues.get(0).toString();
					String destination = entry.getByName("dest").get(0).toString();
					// Get node name from address
					destination = addressToNodeMap.get(destination);
	        		//System.out.println(destination);

					// Get number and unit
					String deliminer = "(?<=\\d)(?=\\D)|(?=\\d)(?<=\\D)";
				
				
					String[] tokens = entry.getByName("start").get(0).toString().split(deliminer);
					String start_value = tokens[0];
					String start_unit = tokens[1];
					
					// Get queue
					int queue = Integer.parseInt(entry.getByName("queue").get(0).toString());
					
					// Get Size
					String strSize = entry.getByName("size").get(0).toString();
					strSize = strSize.split(deliminer)[0];
					int size = Integer.parseInt(strSize);
					
					// Get period
					tokens = xmlNode.getByName("cycle").get(0).toString().split(deliminer);
					String period_value = tokens[0];
					ArrayList<String>path = GetFlowPath(source, destination);
					
					
					addEntry(source, destination, start_value, queue, size, period_value,path);
	        		//System.out.println(source + ", " + destination  + ", " + start_value  + ", " + queue + ", " + size + ", " + period_value + ", " + path);

				}
			}
		}catch (Exception e) {

			return false;
		}
		return true;
		
	}

	public String[] GetNumberAndUnit(String string){
		// Get number and unit
		String deliminer = "(?<=\\d)(?=\\D)|(?=\\d)(?<=\\D)";
		String[] tokens = string.split(deliminer);
		return tokens;
	}
	
	public ArrayList<String> GetFlowPath(String source, String Destination) {
		ArrayList<String> path = new ArrayList<String>();
		String currentNode, DestinationAddress = nodeToAddressMap.get(Destination);
		//Get the first connection from source

		int attempts = 0;
		
		currentNode = networkNode.getByName(source).get(0).toString();
        

		path.add(currentNode);
		
		while(!currentNode.equals(Destination)) {
	        //System.out.println(currentNode);

			// Find switch in routing XML
			attempts++;
			if(xmlRout.children.size()==0 || attempts>100) {
				break; //No rout to take
			}
			for (XMLNode node : xmlRout.children) {
				//if the node 
				if(node.attributesvalues.get(0).equals(currentNode)) {
					
					
					XMLNode forwardNode = node.getByName("static").getByName("forward");
					for (XMLNode forward : forwardNode.children) {
						if(forward.isComment){
							continue;
						}
						if(forward.attributesvalues.get(forward.getAttributIndex("macAddress")).equals(DestinationAddress)) {
							String port = forward.attributesvalues.get(forward.getAttributIndex("port"));
							
							//Check connections for the port
							for (XMLNode connection : networkNode.getByName(currentNode).children) {
								if(connection.attributesvalues.get(0).equals(port)) {
									currentNode = connection.toString();
									path.add(currentNode);
									break;
								}	
							}
							break;
						}
					}
					break;
				}
			}
		}
		return path;
	}
	public ArrayList<String> GetFlowPath2(String source, String Destination, String [] pathSwitches) {
		ArrayList<String> path = new ArrayList<String>();
		String currentSwitch = networkNode.getByName(source).get(0).toString();
	
		//Get the first connection from source
		
		int hopCount = 0;
		path.add(currentSwitch);
		hopCount++;

		while(!currentSwitch.equals(Destination)) {
			
			ArrayList<XMLNode> children = networkNode.getByName(currentSwitch).children;
			//System.out.println(children.toString());
			
			for (XMLNode node : children) 
			{	
				////////////////////////////
				for(int i = 0; i < pathSwitches.length; i++) {
					if (pathSwitches[i] != null){
						if (node.toString().equals(pathSwitches[i])){
							if (!currentSwitch.toString().equals(pathSwitches[i])){
								if(!path.contains(pathSwitches[i])) {

										currentSwitch = node.toString();
										path.add(currentSwitch);
										
										//System.out.println(currentSwitch + ":" + pathSwitches[i]);	
										//System.out.println(source + ":" + Destination +":"+path.toString());
										//System.out.println(grandchildren.toString());

								}
							}
						}

					}
				}
				/////////////////////////////
				if(node.toString().equals(Destination)) {
						path.add(node.toString());
						currentSwitch = Destination;
						System.out.println(source + ":" + Destination +":"+path.toString());

				}
				//else if (node.toString().equals(pathSwitches[hopCount])){
						//System.out.println(node.toString());
				//	path.add(node.toString());
				//	currentSwitch = node.toString();
				//	System.out.println(source +":"+path.toString());
				//}
							
			}

		}
		

		return path;
	}
	public XMLNode read_XML_to_XMLNodefile(File inputFile){
		XMLNode root = null;
	    try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();

			root = new XMLNode("");
			if (doc.hasChildNodes()) {
				root.children = getNodes(doc.getChildNodes());
				root = root.get(0); // Get schedule as root
			}
			else{
				root = new XMLNode("Empty");
			}
	   }
	   catch(Exception e){
		   root = new XMLNode("Empty");
	   }
	   return root;
	}
	
	private ArrayList<XMLNode> getNodes(NodeList nodeList){
		ArrayList<XMLNode> nodes = new ArrayList<>();
		for (int count = 0; count < nodeList.getLength(); count++) {
			Node tempNode = nodeList.item(count);
			XMLNode xmlNode = new XMLNode("");
			// make sure it's element node.
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				xmlNode.string += tempNode.getNodeName();
				if (tempNode.hasAttributes()) {
					// get attributes names and values
					NamedNodeMap nodeMap = tempNode.getAttributes();
					for (int i = 0; i < nodeMap.getLength(); i++) {
						Node node = nodeMap.item(i);
						// If this is a mac address, change the '-' to ':'
						if(node.getNodeName().equals("macAddress"))
							xmlNode.addAttributes(node.getNodeName(), node.getNodeValue().replaceAll("-", ":"));
						else
							xmlNode.addAttributes(node.getNodeName(), node.getNodeValue());
					}
				}
				if (tempNode.hasChildNodes()) {
					// loop again if has child nodes
					xmlNode.children = getNodes(tempNode.getChildNodes());
				}
				nodes.add(xmlNode);
			}
			else if (tempNode.getNodeType() == Node.TEXT_NODE || tempNode.getNodeType() == Node.COMMENT_NODE ) {	
				String s = tempNode.getTextContent().trim();
				if(!s.isEmpty()){
					xmlNode.string = s;
					xmlNode.isComment = tempNode.getNodeType() == Node.COMMENT_NODE; // Track if this is a comment
					nodes.add(xmlNode);
				}
			}
		}
		return nodes;
	}
	
	
	public Boolean read_INI_NED_Files(){
	    try {

			IWorkspace workspace= ResourcesPlugin.getWorkspace();
			
			IPath pathini = org.eclipse.core.runtime.Path.fromOSString(iniFile.getAbsolutePath());
			IPath pathned = org.eclipse.core.runtime.Path.fromOSString(nedFile.getAbsolutePath());
			
			
			
			IFile iFileini = workspace.getRoot().getFileForLocation(pathini);
			IFile iFilened = workspace.getRoot().getFileForLocation(pathned);
			
			
			
			SimplifiedInifileDocument doc = new SimplifiedInifileDocument(iFileini);
			doc.parse();
			IDocument idoc;
			
			ITextFileBufferManager bufferManager = FileBuffers.getTextFileBufferManager();
			
			
			
			
			//NedFileElementEx fileelement = NedResources.getInstance().getNedFileElement(iFileini);
			
			
			
			
			String[] iniStringLines = doc.getContents().split("\n");
			
			
			// Init new lists and maps
			nodeToAddressMap = new HashMap<String, String>();
			addressToNodeMap = new HashMap<String, String>();
			nodeToTypeMap = new HashMap<String, TSNNodeType>();
			hosts_sched = new ArrayList<String>();
			hosts_nonsched = new ArrayList<String>();
			switches = new ArrayList<String>();
			switchMap = new HashMap<>();
			
			
			Pattern pattern = Pattern.compile("([^\\.]*\\.eth\\.address)");
			
			
			for (String string : iniStringLines) {
				String[] temp = string.split("=");
				// Add line if this is an address assignment
	
				if(temp.length==2) {
					String str = temp[0];
					Matcher matcher = pattern.matcher(str);
					if(matcher.find()) {
						str = matcher.group(1).replaceAll(".eth.address", ""); // Remove eth.address  
						String address = temp[1].trim().replaceAll("\"", "");
						address = address.replaceAll("-", ":"); //make the address comply with XML file
						nodeToAddressMap.put(str, address);
						addressToNodeMap.put(address, str);
					}
				}
			}
			
			if(!NedResourcesPlugin.getNedResources().containsNedFileElement(iFilened))
			{
				// File is not loaded
				
			}
			//String content = new String ( Files.readAllBytes(Paths.get(iFilened.getLocationURI())));
			//INedElement  nedfileelement = NedResourcesPlugin.getNedResources().getParsedNedExpression(content);

			
			NedFileElement nedfileelement = null;
			while(nedfileelement==null) {
				try {
					nedfileelement = (NedFileElement)NedResourcesPlugin.getNedResources().getNedFileElement(iFilened);
				}catch (Exception e) {

				}
			}

			List<INedElement> children = nedfileelement.getChildren();
			String output = nedfileelement.getAttribute(0);
			
			CompoundModuleElementEx compound = (CompoundModuleElementEx)children.get(children.size()-1);
			
			networkNode = new XMLNode("");
			
			List<INedElement> temp;
			temp = compound.getChildrenWithTag(NedElementTags.NED_SUBMODULES).get(0).getChildren();
			List<SubmoduleElementEx> submodules = new ArrayList<SubmoduleElementEx>();
			for (INedElement iNedElement : temp) {
				if(iNedElement instanceof CommentElement){
					continue;
				}
				submodules.add((SubmoduleElementEx)iNedElement);
				String name = ((SubmoduleElementEx)iNedElement).getName();
				networkNode.add(new XMLNode(name));
				
				// Sort the type of module
				switch(((SubmoduleElementEx)iNedElement).getType()) {
				case "VlanEtherSwitchPreemptable":
					// Switch
					switches.add(name);

					nodeToTypeMap.put(name, TSNNodeType.SWITCH);
					break;
				case "VlanEtherHostFullLoad": //This does not exist in the newest 
				case "VlanEtherHostQ":
					// Non-Schedulable host
					hosts_nonsched.add(name);
					nodeToTypeMap.put(name, TSNNodeType.NON_SCHED_HOST);
					break;
				case "VlanEtherHostSched":
					// Schedulable host
					hosts_sched.add(name);
					nodeToTypeMap.put(name, TSNNodeType.SCHED_HOST);
					break;
				default:
					break;
				}
				
			}
			
			temp = compound.getChildrenWithTag(NedElementTags.NED_CONNECTIONS).get(0).getChildren();
			List<ConnectionElementEx> connections = new ArrayList<ConnectionElementEx>();
			for (INedElement iNedElement : temp) {
				if(iNedElement instanceof CommentElement){
					continue; //Skip comments
				}
				connections.add((ConnectionElementEx)iNedElement);
				
				// Find for one direction first
				String source = ((ConnectionElementEx)iNedElement).getDestModule();
				String destination = ((ConnectionElementEx)iNedElement).getSrcModule();
				XMLNode node = new XMLNode(destination);
				String port = ((ConnectionElement) iNedElement).getDestGateIndex();
				node.addAttributes("port", port);
				networkNode.getByName(source).add(node);
				
				// Does it go both ways?
				if(((ConnectionElementEx)iNedElement).getIsBidirectional()){
					// Add the other direction also'
					node = new XMLNode(source);
					port = ((ConnectionElement) iNedElement).getSrcGateIndex();
					node.addAttributes("port", port);
					networkNode.getByName(destination).add(node);
				}
			}
	

	

			return true;
				
	   }
	   catch(Exception e){ 	

			networkNode = new XMLNode("");
			networkNode.add(new XMLNode("ERROR : " + e.toString()));
			return false;
	   }
	}
	/*
	<switch name="switchB">
	<cycle>400us</cycle>
	<port id="0">
		<entry>
			<length>400us</length>
			<bitvector>11111111</bitvector>
		</entry>
	</port>
	</switch>
	 */
	// Get information about the switch from XML schedule
	public boolean addSwitchInformation(){
		
		for (String nodeName : switches) {
			try{
			TSNSwitch tsnSwitch = new TSNSwitch(nodeName);
			
			XMLNode node = networkNode.getByName(nodeName);
			
			// Is the switch in the xml sched?
			XMLNode schedNode = null;
			ArrayList<XMLNode> array = xmlSched.getAllWithName("switch");
			if(array != null){
				for (XMLNode xmlEntry : array) {
					if(xmlEntry.getAttributeByName("name").equals(nodeName)){
						// Found the switch in schedule
						schedNode = xmlEntry;
						break;
					}
				}
			}
			
			// Add every port to the switchMap
			ArrayList<XMLNode> portConnections = node.children;
			for (XMLNode xmlNode : portConnections) {
				String str_port = xmlNode.attributesvalues.get(xmlNode.getAttributIndex("port"));
				int int_port = Integer.parseInt(str_port);
				tsnSwitch.addPort(int_port,xmlNode.toString());
			}
			
			if(schedNode==null){
				// The switch is not present on the xml sched

			}
			else{

				// Get entries for each port
				for (XMLNode portNode : schedNode.getAllWithName("port")) {
					// Get cycleTime for the port
					XMLNode sched = portNode.getByName("schedule");
					String str_cycleTime = sched.getAttributeByName("cycleTime");
					int int_port = Integer.parseInt(portNode.getAttributeByName("id"));
					
					tsnSwitch.getPort(int_port).setCycleFromXMLNode(Integer.parseInt(GetNumberAndUnit(str_cycleTime)[0]));
					
					
					for (XMLNode entryNode : sched.children) {
						String strcycle = entryNode.getByName("length").get(0).toString();
						
						// Assume the unit is us, discarding the unit string
						int cycle = Integer.parseInt(GetNumberAndUnit(strcycle)[0]);
						String queueState = entryNode.getByName("bitvector").get(0).toString();
						tsnSwitch.addEntry(int_port, cycle, queueState);
					}
				}
			}

			switchMap.put(nodeName, tsnSwitch);
			
			}catch(Exception e){
				return false;
			}
		}
		return true;
	}

	public static Integer tryParse(String text) {
		  try {
		    return Integer.parseInt(text);
		  } catch (NumberFormatException e) {
		    return 0;
		  }
		}
	/*
	<switch name="switchB">
	<cycle>400us</cycle>
	<port id="0">
		<entry>
			<length>400us</length>
			<bitvector>11111111</bitvector>
		</entry>
	</port>
	</switch>
	 */
		// Get information about the switch from CSV parsed data
	public boolean addSwitchInformation2(ArrayList<String []> gateStates){
			
		System.out.println(gateStates.size());
		
		String type = new String();
		String source = new String();
		String destination = new String();
		String timeStamp = new String();
		String states = new String();
			
		//System.out.println("recheck");
		for(String str_switch : switches){
			try{
				TSNSwitch tsnSwitch = new TSNSwitch(str_switch);
				
				XMLNode switchNode = networkNode.getByName(str_switch);
				
				// Is the switch in the xml sched?
				XMLNode schedNode = null;
				ArrayList<XMLNode> array = xmlSched.getAllWithName("switch");
				if(array != null){
					for (XMLNode xmlEntry : array) {
						if(xmlEntry.getAttributeByName("name").equals(str_switch)){
							// Found the switch in schedule
							schedNode = xmlEntry;
							break;
						}
					}
				}
				
				// Add every port to the switchMap
				ArrayList<XMLNode> portConnections = switchNode.children;
				for (XMLNode xmlNode : portConnections) {
					String str_port = xmlNode.attributesvalues.get(xmlNode.getAttributIndex("port"));
					int int_port = Integer.parseInt(str_port);
					
					tsnSwitch.addPort(int_port,xmlNode.toString());
				}

				System.out.println(str_switch);
			
				int curTime = 0;
				int prvTime = 0; 
				int cycle = 0;

				int count = 0;

			for (String[] data : gateStates) {
					
					type = data[1];
					source =  data[2];
					destination =  data[3];
					timeStamp =  data[4];
					states =  data[5];
			
					if (source.equals(str_switch)){
						//check port
						ArrayList<XMLNode> connectedNodes = switchNode.children;
						for (XMLNode xmlNode : connectedNodes) {
							
								String str_port = xmlNode.attributesvalues.get(xmlNode.getAttributIndex("port"));
								int int_port = Integer.parseInt(str_port);

								if (xmlNode.toString().equals(destination)) {
									curTime = tryParse(timeStamp);

									if (prvTime > curTime)
									{							
										prvTime = 0;
										cycle = 0;
									}

									cycle = curTime - prvTime;
						
									if (cycle!= 0) {
										//state changes still need to be edited
										System.out.println(String.valueOf(count++)+":"+ type +", "+ source + ", (" + xmlNode.toString()  + "):" + int_port +", "+  String.valueOf(cycle) +", "+curTime+", "+ states);

										tsnSwitch.addEntry(int_port, cycle, states);
									}
									
									prvTime =  curTime;

								}
						}
					}
				}
			switchMap.put(str_switch, tsnSwitch);
			//System.out.println("Number of SWs: " + String.valueOf(switchMap.size()));
			}catch(Exception e){
				return false;
			}
		}
	
		return true;
	}
	//////////////////////
	//Parse the information in the synthesisFile
	public Boolean parseFlowSynthesis(File inputFile) {

		
		int maxFlows = 500;
		String[][] data = new String [maxFlows][];
		//ArrayList<String []> data = new ArrayList<String []>();

		String source = new String();
    	String index = new String();
    	String attrTitle = new String();
		String period_value = new String();
    	String deadline = new String();
    	String size = new String();
    	String queue = new String();
    	
    	//String parsedSwitches = "";
    	int MaxHops = 6;
    	String[][] pSwitches = new String [maxFlows][MaxHops];
    	
    	
    	
    	String destination = new String();
    	String type = new String();
    	String start_value = "0";
    	///read the file
		BufferedReader br = null;
		String line = "";
        String cvsSplitBy = ",";
		try{
			br = new BufferedReader(new FileReader(inputFile));
			//read file line by line
	        System.out.println("Flow Entry");
	        int row = -1;
	        
            while ((line = br.readLine()) != null) {
                // use comma as separator
                String[] attribute = line.split(cvsSplitBy);
    			//System.out.println(attribute);

                type = attribute[0];

                if (type.equals("ST"))
                {

                	source = attribute[1];
                	index = attribute[2];
                	attrTitle = attribute[3];
                
                	//String attrContent = attribute[4];
        			//System.out.println(attrTitle);

                	if(attrTitle.equals("period")) {
                		row++;
                		period_value = attribute[4];  
                	}
                	else if(attrTitle.equals("deadline")) {
                		deadline = attribute[4];
                	}
                	else if(attrTitle.equals("length")) {
                		size = attribute[4];
                	}
                	else if(attrTitle.equals("priority")) {
                		queue = attribute[4];
                	}
                	else if(attrTitle.equals("switches")) {
                		int numberOfSW = attribute.length - 4;

                		if (numberOfSW > 1) {

                			for (int i = 0; i < numberOfSW; i++) {
                				//System.out.println(attribute[i+4]);
                				pSwitches[row][i] = attribute[i+4].replace(" ", "");
                			}

                		}else if(numberOfSW == 1){

                			//System.out.println(numberOfSW);
                			//System.out.println(attribute[4]);
                			pSwitches[row][0] = attribute[4];

                		}
                	}
                	else if(attrTitle.equals("dest")) {
                		destination = attribute[4];
                	}

                	data[row]  = new String[] {source, destination, start_value, queue, size, period_value, index, type};
       			 	//data.add(new String[] {source, destination, start_value, queue, size, period_value, index, type});
       			 	//System.out.println(source + ' ' + destination + ' ' + start_value + ' ' + queue + ' ' + size + ' ' + period_value + ' ' + index + ' ' + type);

                }
            }
            int swIndex = 0;
            //System.out.println(data.size());
            int queueInt = 0;
            int sizeInt = 0;
            String indexString = "0";
            ArrayList<String>path = new  ArrayList<String> ();
            for (String[] flow : data) {

                source = flow[0];
                destination = flow[1];
                start_value = flow[2];
                queueInt =  Integer.parseInt(flow[3]);
                sizeInt = Integer.parseInt(flow[4]);
                period_value = flow[5];
                indexString = flow[6];
                type = flow[7];
             
                String [] pathSwitches = pSwitches[swIndex];
                swIndex++;

                path = GetFlowPath2(source, destination, pathSwitches);
    	       	
        		int ByteToBit = 8;
                //int linkSpeed = 1000;
                addEntryAuto(indexString, source, destination, start_value, queueInt, (sizeInt/ByteToBit), period_value, path);  
            
	        }
    	    


        }  
		catch (IOException e) {
            
        	e.printStackTrace();
        
        } finally {
        	
            if (br != null) {
            
            	try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        		
            	return false;
            }
            
        }
		return true;

	}
	
	public ArrayList<String []> parseGateSynthesis(File inputFile) {
        

		ArrayList<String []> data= new ArrayList<String []>();
		
        String type = new String();
        String source = new String();
        String destination = new String();

        String linkID = new String();

        //may be bigger in future
        int maxGatesStates = 50000;
        String[] gateStates = new String [maxGatesStates];

        
        
        String timeStamp = new String();
        ///read the file
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		try{
			br = new BufferedReader(new FileReader(inputFile));
			//read file line by line
	         System.out.println("Gate Synthesis");
	         int row = 0;
		     while ((line = br.readLine()) != null) {

		    	 // use comma as separator
		         String[] attribute = line.split(cvsSplitBy);
		        
		         type = attribute[0].replace(" ", "");
		         if (type.equals("STs")) {
		        	 source = attribute[1].replace("[V_", "").replace("\"", "").replace(" ", "");
		         
		        	 destination = attribute[2].replace("V_", "").replace("]","").replace("\"", "").replace(" ", "");
		        	 linkID = source +", "+ destination;

		        	 //System.out.println("Type: " + type + "| linkID: " + linkID);

		        
		        	 for (int i = 3 ; i < attribute.length ; i++) {
		        		 if( i % 2 != 0) {
		        			 //Save gate opening status
		        			 //at this point we need to change to open for ST
		        			 gateStates[row] = "00001111";//previous cycle: upto this time has been close

		        			 timeStamp = attribute[i].replace(" ", "").replace("[", "").replace("\"", "").replace(".0","");
			        	 
		        			 data.add(new String[] {String.valueOf(row), type,source, destination,timeStamp, gateStates[row]});

		        			 //System.out.println(linkID + "Gates opens: " +  gateStates[row][i]);
		        			 //System.out.println(String.valueOf(row) +":"+linkID + ": Gates open: " +  timeStamp +": "+ gateStates[row]);

		        		 }else {
		        			 //Save gate close status
		        			 //at this point we need to change to close to ST
		        			 gateStates[row] = "11110000";//previous cycle: upto this time has been open		        		 
		        			 timeStamp = attribute[i].replace(" ", "").replace("]", "").replace("\"", "").replace(".0","");
			        	 
		        			 data.add(new String[] {String.valueOf(row), type,source, destination ,timeStamp, gateStates[row]});

		        			 //System.out.println(linkID + "Gates closes: " +  gateStates[row][i]);
		        			 //System.out.println(String.valueOf(row) +":"+linkID + ": Gates close: " +  timeStamp + ": "+ gateStates[row]);

		        		 }
		        		 row++;
		        	 }
		         }
		         
        		 //System.out.println(row);

		       
		            	
		    }
		   
		}  
		catch (IOException e) {
		            e.printStackTrace();
		} 
		finally {
		      
		       try {
		    	   	br.close();
		       } 
		       catch (IOException e) 
		            {
		                    e.printStackTrace();
		       		}
		       	}
		
	return data;
	}

	///////////////set offsets
	
	public Boolean parseOffset(File inputFile) {
			String type = new String();	
			String source = new String();	
			String destination = new String();	
			Double offset;
			BufferedReader br = null;
			String line = "";
			String cvsSplitBy = ",";	
			try{
				br = new BufferedReader(new FileReader(inputFile));
				System.out.println("Offsets Synthesis");
				int row = 0;
			    while ((line = br.readLine()) != null) {

			    	 // use comma as separator
			         String[] attribute = line.split(cvsSplitBy);
			        
			         type = attribute[0].replace(" ", "");
			         if (type.equals("STs")) {
			        	 
			        	 source = attribute[1].replace("[V_", "").replace("\"", "").replace(" ", "");
			        	 destination = attribute[2].replace("V_", "").replace("]","").replace("\"", "").replace(" ", "");
			        	 if(source.contains("_")){
			        		 	String sourceOffset = source.split("_")[0];
			        	 		String sourceIndex = source.split("_")[1];
			        		 	for(Map.Entry<String, ArrayList<FlowListEntry>> Iter : flowPeriods.entrySet()) {
			        		 		String sourceKey = Iter.getKey();
			        		 		ArrayList<FlowListEntry> listOfFlows = Iter.getValue();
			     				    
			        		 		for(FlowListEntry flow : listOfFlows) {
			        		 				
			        		 			if(sourceIndex.equals(flow.getID())) {
			        		 				if(sourceOffset.equals(flow.getSource())) {
			        		 					if(destination.equals(flow.GetPath().get(0))) {
			        		 						offset = Double.parseDouble(attribute[3].replace(" ", "").replace("[", "").replace("\"", "").replace(".0",""));
			        		 						flow.setStart(offset);

			        		 						System.out.println("ID: "+sourceIndex +"| Offset: " + flow.getStart());
			        		 					}//else {System.out.println("No matching destination");}
			        		 				}//else {System.out.println("No matching source");}
			        		 			}// else {System.out.println("No matching ID");}
			        		 		}
			        		 	}
			        	 }
			    
			         }
			    }
			         
			}  
			catch (IOException e) {
        
					e.printStackTrace();
    
			} finally {
    	
				if (br != null) {
        
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
    		
					return false;
				}
        
			}
	return true;

	}

	
	
	
	
	
	//Read the configureations from the synthesisFile

	public Boolean CSV_is_empty(File inputFile) {
		//String[][] data = new String[10][100];
		//int row = 0;
		///read the file
				
		
		BufferedReader br = null;

		try{
			br = new BufferedReader(new FileReader(inputFile));
		            
        }
        catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        		return false;
            }
          
        }
		return true;

	}
	
	//////////////////////
	
	@Override
	public void setFocus() {
		// Set the focus
 	}
}
