package org.plugin.tsnsched.views;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.wb.swt.SWTResourceManager;
import org.plugin.model.SwitchEntry;
import org.plugin.model.SwitchPort;

public class PortDialog extends Dialog {

	public static final String OPEN = "open";
	public static final String CLOSE = "closed";
	public SwitchPort switchPort;
	private Table table;
	private Color COLOR_GREEN;
	private Color COLOR_RED;
	private static String deliminer = " -> ";
	private SetupView setupView;
	private String nodeName;

	public PortDialog(Shell parentShell, SetupView setupView, String nodeName) {
		super(parentShell);
		this.setupView = setupView;
		this.nodeName = nodeName;
		setShellStyle(SWT.RESIZE);
	}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * Create contents of the dialog.
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {

		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new FormLayout());

		COLOR_GREEN = new Color(Display.getDefault(), new RGB(0, 200, 0));
		COLOR_RED = new Color(Display.getDefault(), new RGB(200, 0, 0));
		
		Label lblPortX = new Label(container, SWT.NONE);
		FormData fd_lblPortX = new FormData();
		fd_lblPortX.top = new FormAttachment(0, 13);
		fd_lblPortX.left = new FormAttachment(0, 11);
		lblPortX.setLayoutData(fd_lblPortX);
		lblPortX.setFont(SWTResourceManager.getFont("Segoe UI", 14, SWT.BOLD));
		lblPortX.setText("Port " + switchPort.getID());

		Label lblConnectedTo = new Label(container, SWT.NONE);
		FormData fd_lblConnectedTo = new FormData();
		fd_lblConnectedTo.top = new FormAttachment(lblPortX, 8, SWT.TOP);
		fd_lblConnectedTo.left = new FormAttachment(lblPortX, 45);
		lblConnectedTo.setLayoutData(fd_lblConnectedTo);
		lblConnectedTo.setText("Connected to " + switchPort.getConnectedNode());


		table = new Table(container, SWT.BORDER);
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
		FormData fd_table = new FormData();
		fd_table.top = new FormAttachment(lblPortX, 6);
		fd_table.left = new FormAttachment(lblPortX, 0, SWT.LEFT);
		fd_table.right = new FormAttachment(100, -10);
		table.setLayoutData(fd_table);

		TableColumn tblclmnCycle = new TableColumn(table, SWT.NONE);
		tblclmnCycle.setResizable(false);
		tblclmnCycle.setWidth(110);
		tblclmnCycle.setText("Cycle (us)");

		Button btnAddCycle = new Button(container, SWT.NONE);
		
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		btnAddCycle.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				SwitchEntry entry= new SwitchEntry(100);
				addEntry(entry);
			}
		});

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		fd_table.bottom = new FormAttachment(btnAddCycle, -6);
		FormData fd_btnAddCycle = new FormData();
		fd_btnAddCycle.right = new FormAttachment(0, 86);
		fd_btnAddCycle.bottom = new FormAttachment(100, -5);
		fd_btnAddCycle.left = new FormAttachment(0, 11);
		btnAddCycle.setLayoutData(fd_btnAddCycle);
		btnAddCycle.setText("Add cycle");

		/*
		for(int i = 0; i < 8;i++) {
			TableColumn tblclmnQueue = new TableColumn(table, SWT.NONE);
			tblclmnQueue.setWidth(100);
			tblclmnQueue.setText("Queue " + Integer.toString(i));
		}
		 */
		
		int width = 70;
		String firstPart = "Queue ";
		Boolean resize = true;
		TableColumn tblclmnQueue = new TableColumn(table, SWT.NONE);
		tblclmnQueue.setResizable(false);
		tblclmnQueue.setWidth(width);
		tblclmnQueue.setText(firstPart + Integer.toString(8) + "\nNC  (" + Integer.toString(7) + ")");

		tblclmnQueue = new TableColumn(table, SWT.NONE);
		tblclmnQueue.setResizable(false);
		tblclmnQueue.setWidth(width);
		tblclmnQueue.setText(firstPart + Integer.toString(7) + "\nIC  (" + Integer.toString(6) + ")");

		tblclmnQueue = new TableColumn(table, SWT.NONE);
		tblclmnQueue.setResizable(false);
		tblclmnQueue.setWidth(width);
		tblclmnQueue.setText(firstPart + Integer.toString(6) + "\nVO  (" + Integer.toString(5) + ")");

		tblclmnQueue = new TableColumn(table, SWT.NONE);
		tblclmnQueue.setResizable(false);
		tblclmnQueue.setWidth(width);
		tblclmnQueue.setText(firstPart + Integer.toString(5) + "\nVI  (" + Integer.toString(4) + ")");

		tblclmnQueue = new TableColumn(table, SWT.NONE);
		tblclmnQueue.setResizable(false);
		tblclmnQueue.setWidth(width);
		tblclmnQueue.setText(firstPart + Integer.toString(4) + "\nCA  (" + Integer.toString(3) + ")");

		tblclmnQueue = new TableColumn(table, SWT.NONE);
		tblclmnQueue.setResizable(false);
		tblclmnQueue.setWidth(width);
		tblclmnQueue.setText(firstPart + Integer.toString(3) + "\nEE  (" + Integer.toString(2) + ")");

		tblclmnQueue = new TableColumn(table, SWT.NONE);
		tblclmnQueue.setResizable(false);
		tblclmnQueue.setWidth(width);
		tblclmnQueue.setText(firstPart + Integer.toString(2) + "\nBE  (" + Integer.toString(0) + ")");

		tblclmnQueue = new TableColumn(table, SWT.NONE);
		tblclmnQueue.setResizable(false);
		tblclmnQueue.setWidth(width);
		tblclmnQueue.setText(firstPart + Integer.toString(1) + "\nBK  (" + Integer.toString(1) + ")");
		
		Button btnDiagram = new Button(container, SWT.NONE);
		btnDiagram.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				JDialog dialog = new JDialog(Display.getCurrent().getActiveShell(),switchPort,setupView,nodeName);
				if (dialog.open() == Window.OK){
					// User clicked OK
				}
			}
		});

		FormData fd_btnDiagram = new FormData();
		fd_btnDiagram.bottom = new FormAttachment(lblPortX, 0, SWT.BOTTOM);
		fd_btnDiagram.right = new FormAttachment(table, 0, SWT.RIGHT);
		btnDiagram.setLayoutData(fd_btnDiagram);
		btnDiagram.setText("Diagram");
		
		Label hint = new Label(container, SWT.NONE);
		FormData fd_hint = new FormData();
		fd_hint.bottom = new FormAttachment(btnAddCycle, 0, SWT.BOTTOM);
		fd_hint.left = new FormAttachment(btnAddCycle, 6);
		hint.setLayoutData(fd_hint);
		hint.setText("Right click to remove cycle");
		
	
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//Add all current entries for this port
		for (SwitchEntry entry : switchPort.getEntries()) {
			addEntry(entry);
				
		}
		
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		/*
		TableItem tableItem = new TableItem(table, SWT.NONE);
		tableItem.setText("400");
		tableItem.setText(1, "T");
		tableItem.setText(2, "T");
		tableItem.setText(3, "T");
		tableItem.setText(4, "T");
		tableItem.setText(5, "T");
		tableItem.setText(6, "T");
		tableItem.setText(7, "T");
		tableItem.setText(8, "T");
		 */
		
		
		
		table.addListener(SWT.EraseItem, new Listener() {
			public void handleEvent(Event event) {
				/*
				event.detail &= ~SWT.HOT;
				if ((event.detail & SWT.SELECTED) == 0) return;
				String text = ((TableItem)event.item).getText(event.index);
				GC gc = event.gc;
				if(text.equals(OPEN)) {
					gc.setForeground(COLOR_GREEN);
					gc.setBackground(COLOR_GREEN);
				}
				else if(text.equals(CLOSE)) {
					gc.setForeground(COLOR_RED);
					gc.setBackground(COLOR_RED);
				}
				
		
				event.detail &= ~SWT.SELECTED;
				return;
			
				//if ((event.detail & SWT.SELECTED) == 0) return; /* item not selected */
				
				//GC gc = event.gc;
				//Color oldForeground = gc.getForeground();
				//Color oldBackground = gc.getBackground();
				
				//gc.fillGradientRectangle(0, event.y, clientWidth, event.height, false);
				//gc.setForeground(oldForeground);
				//((TableItem)event.item)
				//gc.setBackground(oldBackground);
				//event.detail &= ~SWT.SELECTED;
				
			}
		});
		
		
		
		table.addListener(SWT.MouseDown, new Listener() {
			public void handleEvent(Event event) {
				// Find the item that was pressed
				Rectangle clientArea = table.getClientArea();
				Point pt = new Point(event.x, event.y);
				int index = table.getTopIndex();
				while (index < table.getItemCount()) {
					boolean visible = false;
					TableItem item = table.getItem(index);
					for (int i = 0; i < table.getColumnCount(); i++) {
						Rectangle rect = item.getBounds(i);
						if (rect.contains(pt)) {
							
							int button = event.button;
							switch(button) {
							case 1://Left mouse button
								if(i==0) {
									// The Cycle column was pressed. Show cycle editor
									int cycle = splitCycle(item);

									InputDialog dialog = new InputDialog(Display.getCurrent().getActiveShell(),
											"", "Enter cycle for this entry", Integer.toString(cycle), new ValueValidator());
									if (dialog.open() == Window.OK){
										// User clicked OK; update Cycle
										item.setText(i, dialog.getValue());
										switchPort.entryList = getEntryList(); //Update the entry list
									}
								}
								else if(i>0){
									// Find the queue and switch the value
									String s = item.getText(i).equals(OPEN) ? CLOSE : OPEN; //T -> F and F -> T
									Color itemColor = s.equals(OPEN) ? COLOR_GREEN : COLOR_RED;
									
									
									item.setText(i, s);
									item.setBackground(i, itemColor);
									switchPort.entryList = getEntryList(); //Update the entry list
								}
								break;
							case 3://Right mouse button:
								table.remove(index);
								switchPort.entryList = getEntryList(); //Update the entry list
								break;
							default:
								break;
							}

						}
						if (!visible && rect.intersects(clientArea)) {
							visible = true;
						}
					}
					if (!visible)
						return;
					index++;
				}
			}
		});



		return container;
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void addEntry(SwitchEntry entry) {
		TableItem tableItem = new TableItem(table, SWT.NONE);
		tableItem.setText(Integer.toString(entry.queueTime));
		Boolean[] states = entry.queueState;
		for (int i = 0; i < 8; i++) {
			String t = states[i] ? OPEN : CLOSE;
			Color itemColor = states[i] ? COLOR_GREEN : COLOR_RED;
			tableItem.setText(i + 1, t);
			tableItem.setBackground(i + 1, itemColor);
		}
		switchPort.entryList = getEntryList(); //Update the entry list
	}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * Create contents of the button bar.
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(700, 300);
	}

	public void setSwitchPort(SwitchPort switchPort) {
		
		try {
			this.switchPort = (SwitchPort) switchPort.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public List<SwitchEntry> getEntryList() {
		ArrayList<SwitchEntry> entryList = new ArrayList<>();
		int time = 0;
		int cycle;
		for (TableItem tableEntry : table.getItems()) {
			// Get cycle for the entry
			cycle = splitCycle(tableEntry);
			SwitchEntry entry = new SwitchEntry(cycle);
			// Set cycle text
			String text = Integer.toString(time) + deliminer + Integer.toString(time+cycle) + " :" + Integer.toString(cycle) +":" ;
			time += cycle; //Increase time for the next entry
			tableEntry.setText(text);
			for (int i = 0; i < 8; i++) {
				entry.queueState[i] = tableEntry.getText(i+1).equals(OPEN);
			}
			entryList.add(entry);
		}
		return entryList;
	}
	//Gives the cycle for a table entry
	public static int splitCycle(TableItem tableEntry) {
		String cycleText = tableEntry.getText(0);
		
		String[] tokens = cycleText.split(deliminer);
		
		if(tokens.length == 1) {
			//The cycle does not have interval, return as is
			return Integer.parseInt(tokens[0]);
		}
		else if(tokens.length == 2) {
			//give the 
			int a,b;
			a = Integer.parseInt(tokens[0]);
			String[] token2 = tokens[1].split(" :");
			b = Integer.parseInt(token2[0]);
			return b-a;
		}
		//Error, the string was not processed correctly
		return -1;
	}
}
