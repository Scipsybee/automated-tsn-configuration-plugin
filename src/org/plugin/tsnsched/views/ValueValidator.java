package org.plugin.tsnsched.views;

import org.eclipse.jface.dialogs.IInputValidator;

public class ValueValidator implements IInputValidator {

	@Override
	public String isValid(String arg0) {
		int i;
		try {
			i = Integer.parseInt(arg0);
		}catch (Exception e) {
			return "Must only contain numbers" ;
		}
		if(i<0) {return "Can't be smaller than 0";}
		return null;
	}

}
