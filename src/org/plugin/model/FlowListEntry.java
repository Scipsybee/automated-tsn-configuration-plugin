package org.plugin.model;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.wb.swt.SWTResourceManager;
import org.plugin.tsnsched.views.SetupView;

public class FlowListEntry {
	public Composite composite;
	private Label lblQueue,lblPath,lblRequirements;
	private Combo cboQueue;
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	private Label lblNewLabel_2;
	private Label lblNewLabel_3;
	private SashForm sashForm_options;
	
	private Text text_period;
	private String string_period;

	
	private Group grpSize;
	private Text text_size;
	private Label lblBytes;
	private Group grpStartDelay;
	private Text text_start;
	private Label lblMs_2;
	private int lastPathNode;
	public Button btnRemove, btnAddNode;
	private Composite sashForm_path;
	
	private String source;
	private String destination;
	private int queue;
	private int frameSize;
	private String id;


	private ArrayList<String> nodePath = new ArrayList<String>();
	
	public FlowListEntry(Composite parent, String source, String destination, String start, int queue, int size, String period_value, ArrayList<String> path, String id) {
		this.source = source;
		this.destination = destination;
		this.queue = queue;
		this.frameSize = size;
		this.id = id;
		
		composite = new Composite(parent, SWT.BORDER);
		GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		//gd_composite.widthHint = 700;
		composite.setLayoutData(gd_composite);
		//composite.setSize(510, 130);
		composite.setSize(660, 160);
		composite.setLayout(new FormLayout());
		
		toolkit.adapt(composite);
		toolkit.paintBordersFor(composite);
		
		


		composite.setLayout(new FormLayout());
		
		lblQueue = new Label(composite, SWT.NONE);
		lblQueue.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		lblQueue.setAlignment(SWT.CENTER);
		FormData fd_lblQueue = new FormData();
		fd_lblQueue.right = new FormAttachment(0, 67);
		lblQueue.setLayoutData(fd_lblQueue);

		lblQueue.setText("Queue: ");
		
		lblPath = new Label(composite, SWT.NONE);
		fd_lblQueue.left = new FormAttachment(lblPath, 0, SWT.LEFT);
		lblPath.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		FormData fd_lblPath = new FormData();
		fd_lblPath.top = new FormAttachment(0, 10);
		fd_lblPath.left = new FormAttachment(0, 10);
		lblPath.setLayoutData(fd_lblPath);

		lblPath.setText("Path:");
		
		lblRequirements = new Label(composite, SWT.NONE);
		lblRequirements.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		FormData fd_lblNewLabel = new FormData();
		fd_lblNewLabel.bottom = new FormAttachment(lblQueue, -22);
		fd_lblNewLabel.left = new FormAttachment(lblQueue, 0, SWT.LEFT);
		lblRequirements.setLayoutData(fd_lblNewLabel);

		lblRequirements.setText("Requirements:");
		
		cboQueue = new Combo(composite, SWT.NONE);
		fd_lblQueue.top = new FormAttachment(cboQueue, -2, SWT.TOP);
		cboQueue.setItems(new String[] {"1", "2", "3", "4", "5", "6", "7", "8"});
		FormData fd_combo = new FormData();
		fd_combo.bottom = new FormAttachment(100);
		fd_combo.left = new FormAttachment(lblQueue, 6);
		cboQueue.setLayoutData(fd_combo);

		cboQueue.select(queue);
		
		toolkit.adapt(lblQueue, true, true);
		toolkit.adapt(lblPath, true, true);
		toolkit.adapt(lblRequirements, true, true);
		toolkit.adapt(cboQueue);
		toolkit.paintBordersFor(cboQueue);
		toolkit.adapt(lblRequirements, true, true);
		
		sashForm_path = new SashForm(composite, SWT.NONE);
		fd_lblNewLabel.top = new FormAttachment(sashForm_path, 21);
		FormData fd_sashForm = new FormData();
		/*
		fd_sashForm.left = new FormAttachment(lblPath, 10);
		fd_sashForm.right = new FormAttachment(100, -10);
		fd_sashForm.top = new FormAttachment(0, 8);
		*/
		fd_sashForm.left = new FormAttachment(0, 53);
		fd_sashForm.top = new FormAttachment(0, 8);
		sashForm_path.setLayoutData(fd_sashForm);
		toolkit.adapt(sashForm_path);
		toolkit.paintBordersFor(sashForm_path);
		

		
		
		
		
		
		
		
		
		
		
		lastPathNode = 0;
		Label lblNewLabel = new Label(sashForm_path, SWT.BORDER);
		lblNewLabel.setAlignment(SWT.CENTER);
		lblNewLabel.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		toolkit.adapt(lblNewLabel, true, true);
		lblNewLabel.setText(source);
		
		// Add one lable for each node in the path
		for (String string : path) {
			AddPathNode(string);
		}
		
		btnAddNode = new Button(composite, SWT.NONE);
		fd_sashForm.right = new FormAttachment(100, -73);
		FormData fd_btnAddNode = new FormData();
		fd_btnAddNode.right = new FormAttachment(100, -3);
		fd_btnAddNode.top = new FormAttachment(0, 8);
		btnAddNode.setLayoutData(fd_btnAddNode);
		
		toolkit.adapt(btnAddNode, true, true);
		btnAddNode.setText("Add node");
		
		sashForm_options = new SashForm(composite, SWT.NONE);
		FormData fd_sashForm_1 = new FormData();
		fd_sashForm_1.left = new FormAttachment(lblRequirements, 6);
		fd_sashForm_1.top = new FormAttachment(sashForm_path, 13);
		fd_sashForm_1.right = new FormAttachment(100, -10);
		fd_sashForm_1.bottom = new FormAttachment(100, -34);
		sashForm_options.setLayoutData(fd_sashForm_1);
		toolkit.adapt(sashForm_options);
		toolkit.paintBordersFor(sashForm_options);
		
		Group grpCyclePeriod = new Group(sashForm_options, SWT.NONE);
		grpCyclePeriod.setText("Cycle period");
		toolkit.adapt(grpCyclePeriod);
		toolkit.paintBordersFor(grpCyclePeriod);
		grpCyclePeriod.setLayout(new FormLayout());
		
		text_period = new Text(grpCyclePeriod, SWT.BORDER);
		FormData fd_text_2 = new FormData();
		fd_text_2.top = new FormAttachment(0, 3);
		fd_text_2.left = new FormAttachment(0, 3);
		text_period.setLayoutData(fd_text_2);
		toolkit.adapt(text_period, true, true);
		
		text_period.setText(period_value);
		string_period = period_value;
		
		Label lblMs = new Label(grpCyclePeriod, SWT.NONE);
		fd_text_2.right = new FormAttachment(lblMs, -6);
		FormData fd_lblMs = new FormData();
		fd_lblMs.top = new FormAttachment(0, 6);
		fd_lblMs.right = new FormAttachment(100, -10);
		lblMs.setLayoutData(fd_lblMs);
		toolkit.adapt(lblMs, true, true);
		lblMs.setText("us");
		
		grpSize = new Group(sashForm_options, SWT.NONE);
		grpSize.setText("Size");
		toolkit.adapt(grpSize);
		toolkit.paintBordersFor(grpSize);
		grpSize.setLayout(new FormLayout());
		
		text_size = new Text(grpSize, SWT.BORDER);
		FormData fd_text_3 = new FormData();
		fd_text_3.top = new FormAttachment(0, 3);
		fd_text_3.left = new FormAttachment(0, 3);
		text_size.setLayoutData(fd_text_3);
		toolkit.adapt(text_size, true, true);
		text_size.setText(Integer.toString(size));
		
		lblBytes = new Label(grpSize, SWT.NONE);
		fd_text_3.right = new FormAttachment(lblBytes, -6);
		lblBytes.setText("Bytes");
		FormData fd_lblBytes = new FormData();
		fd_lblBytes.top = new FormAttachment(0, 6);
		fd_lblBytes.right = new FormAttachment(100, -10);
		lblBytes.setLayoutData(fd_lblBytes);
		toolkit.adapt(lblBytes, true, true);
		
		grpStartDelay = new Group(sashForm_options, SWT.NONE);
		grpStartDelay.setText("Start delay");
		toolkit.adapt(grpStartDelay);
		toolkit.paintBordersFor(grpStartDelay);
		grpStartDelay.setLayout(new FormLayout());
		
		text_start = new Text(grpStartDelay, SWT.BORDER);
		FormData fd_text_4 = new FormData();
		fd_text_4.left = new FormAttachment(0, 3);
		fd_text_4.top = new FormAttachment(0, 3);
		text_start.setLayoutData(fd_text_4);
		text_start.setText(start);
		toolkit.adapt(text_start, true, true);
		
		lblMs_2 = new Label(grpStartDelay, SWT.NONE);
		fd_text_4.right = new FormAttachment(lblMs_2, -12);
		lblMs_2.setText("us");
		FormData fd_lblMs_2 = new FormData();
		fd_lblMs_2.top = new FormAttachment(text_start, 3, SWT.TOP);
		fd_lblMs_2.right = new FormAttachment(100, -10);
		lblMs_2.setLayoutData(fd_lblMs_2);
		toolkit.adapt(lblMs_2, true, true);
		
		btnRemove = new Button(composite, SWT.NONE);
		fd_sashForm_1.bottom = new FormAttachment(btnRemove, -18);
		btnRemove.setSelection(true);
		FormData fd_btnRemove = new FormData();
		fd_btnRemove.bottom = new FormAttachment(100, -2);
		fd_btnRemove.right = new FormAttachment(btnAddNode, 0, SWT.RIGHT);
		btnRemove.setLayoutData(fd_btnRemove);
		toolkit.adapt(btnRemove, true, true);
		btnRemove.setText("Remove");
		
	}
    //@Override
	//public Double compareTo(FlowListEntry compareFlow) {
	//    Double compareStart=((FlowListEntry)compareFlow).getStart();
	//    /* For Ascending order*/
	//    return Double.parseDouble(text_start.getText()) - compareStart;

	//    /* For Descending order do like this */
	//    //return compareage-this.studentage;
	//}
	
	
	public String GetLatestPathNode() {
		// Return first node if list is empty
		if(nodePath.size()==0)
			return ((Label)sashForm_path.getChildren()[0]).getText();
		else
			return nodePath.get(nodePath.size()-1);
	}
	public void AddPathNode(String node) {
		Label lblpath = new Label(sashForm_path, SWT.BORDER);
		lblpath.setAlignment(SWT.CENTER);
		lblpath.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		toolkit.adapt(lblpath, true, true);
		lblpath.setText(node);
		nodePath.add(node);
		int thisNode = ++lastPathNode;
		sashForm_path.layout();

		// Remove node when double clicked if it is the last
		lblpath.addListener(SWT.MouseDoubleClick, new Listener() {
			
			@Override
			public void handleEvent(Event event) {
				// Remove only if this is the last node in the list
				if(thisNode == lastPathNode) {
					lastPathNode--;
					lblpath.dispose();
					sashForm_path.layout();
					nodePath.remove(nodePath.size()-1); // Remove last
					btnAddNode.setEnabled(true); // This will always be true after removing a node
				}
			}
		});
	}
	public ArrayList<String> GetPath(){
		return nodePath;
	}
	public XMLNode toXMLHost() {
		XMLNode node = new XMLNode("host");
		node.addAttributes("name", source);
		
		XMLNode cycle = new XMLNode("cycle");
		cycle.add(new XMLNode(text_period.getText() + "us"));
		
		node.add(cycle);
		
		return node;
	}
	public XMLNode toXMLEntry() {
		XMLNode entry = new XMLNode("entry");
		
		XMLNode start = new XMLNode("start");
		start.add(new XMLNode(text_start.getText() + "us"));
		
		XMLNode queue = new XMLNode("queue");
		String queueNumber = Integer.toString(cboQueue.getSelectionIndex());
		queue.add(new XMLNode(queueNumber));
		
		XMLNode dest = new XMLNode("dest");
		dest.add(new XMLNode(nodePath.get(nodePath.size()-1)));
		
		XMLNode size = new XMLNode("size");
		size.add(new XMLNode(text_size.getText() + "B"));
		
		entry.add(start);
		entry.add(queue);
		entry.add(dest);
		entry.add(size);
		
		return entry;
	}
	public int getSize() {
		return frameSize;
	}
	public int getQueue() {
		return queue;
	}
	public String getDestination() {
		return destination;
	}
	public String getSource() {
		return source;
	}
	public double getCycle() {
		return Double.parseDouble(text_period.getText());
	}
	public String getCycleStr() {
		return string_period;
	}
	public Double getStart() {
		return Double.parseDouble(text_start.getText());
	}
	public void setStart(Double offset) {
		this.text_start.setText(String.valueOf(offset));
	}
	public String getID() {
		return id;
	}
	public void setID(String id) {
		this.id = id;
	}
}
