package org.plugin.model;

public final class MathOperations {
	public static double gcd(double a, double b)
	{
	    while (b > 0)
	    {
	        double temp = b;
	        b = a % b;
	        a = temp;
	    }
	    return a;
	}

	public static double gcd(double[] input)
	{
	    double result = input[0];
	    for(int i = 1; i < input.length; i++) result = gcd(result, input[i]);
	    return result;
	}
	
	public static double lcm(double a, double b)
	{
	    return a * (b / gcd(a, b));
	}

	public static double lcm(double[] input)
	{
	    double result = input[0];
	    for(int i = 1; i < input.length; i++) result = lcm(result, input[i]);
	    return result;
	}
	public static double lcm(Double[] input)
	{
		Double result = input[0];
	    for(int i = 1; i < input.length; i++) 
	    {
	    	if (input[i] != null)
	    	{
	    		result = lcm(result, input[i]);
	    	}
	   	}
	    return result;
	}
}
