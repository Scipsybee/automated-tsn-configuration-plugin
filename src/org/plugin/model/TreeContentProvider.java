package org.plugin.model;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;

public class TreeContentProvider implements ITreeContentProvider {
	  @Override 
	  public void dispose() { 
	 
	  } 
	 
	  @Override 
	  public void inputChanged(Viewer viewer, Object oldInput, Object newInput) { 
	 
	  } 
	 
	  @Override 
	  public Object[] getChildren(Object parentElement) {
		  if(parentElement instanceof XMLNode) {
			  XMLNode box = (XMLNode)parentElement;
		        return box.children.toArray();
		    }
		    return null;
	  } 
	 
	  @Override 
	  public Object[] getElements(Object inputElement) {
		    return getChildren(inputElement);
	  } 
	 
	  @Override 
	  public Object getParent(Object element) { 
	    return null; 
	  } 
	 
	  @Override 
	  public boolean hasChildren(Object element) {
	    return getChildren(element).length > 0;
	  } 

}
