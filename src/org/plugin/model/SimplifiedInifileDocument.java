package org.plugin.model;


import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.omnetpp.inifile.editor.model.InifileParser;
import org.omnetpp.inifile.editor.model.InifileDocument;
import org.omnetpp.inifile.editor.model.InifileParser.ParserCallback;

public class SimplifiedInifileDocument {
    private IFile file;
    private Map<String,String> keyValueMap = new HashMap<String, String>();
    
    private class Callback implements InifileParser.ParserCallback {
        @Override
        public void blankOrCommentLine(int lineNumber, int numLines, String rawLine, String rawComment) {
            // ignore
        }

        @Override
        public void sectionHeadingLine(int lineNumber, int numLines, String rawLine, String sectionName, String rawComment) {
            if (!sectionName.equals("General"))
                parseError(lineNumber, numLines, "Only the [General] section is supported");
        }

        @Override
        public void keyValueLine(int lineNumber, int numLines, String rawLine, String key, String value, String rawComment) {
            keyValueMap.put(key, value);
        }

        @Override
        public void directiveLine(int lineNumber, int numLines, String rawLine, String directive, String args, String rawComment) {
            parseError(lineNumber, numLines, "Directive lines such as \"include\" are not supported");
        }

        @Override
        public void parseError(int lineNumber, int numLines, String message) {
            try {
				throw new IOException();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
        
    }

    public SimplifiedInifileDocument() {
    }
    
    public SimplifiedInifileDocument(IFile file) {
        this.file = file;
    }
    
    public void parse() throws CoreException, IOException {
        if (file == null)
            throw new RuntimeException("no file given");
        new InifileParser().parse(file, new Callback());
    }
    
    public IFile getDocumentFile() {
        return file;
    }
    
    public void setValue(String key, String value) {
        keyValueMap.put(key, value);
    }

    public String getValue(String key) {
        return keyValueMap.get(key);
    }

    public void removeValue(String key) {
        keyValueMap.remove(key);
    }

    public String getContents() throws CoreException {
        String[] keys = keyValueMap.keySet().toArray(new String[]{});
        Arrays.sort(keys);

        StringBuilder result = new StringBuilder();
        result.append("[General]\n");
        for (String key : keys) {
            result.append(key);
            result.append(" = ");
            result.append(keyValueMap.get(key));
            result.append("\n");
        }
        return result.toString();
    }
    
    public String toString() {
        String[] keys = keyValueMap.keySet().toArray(new String[]{});
        Arrays.sort(keys);

        StringBuilder result = new StringBuilder();
        for (String key : keys) {
            result.append(key);
            result.append(" = ");
            result.append(keyValueMap.get(key));
            result.append("\n");
        }
        return result.toString();
    }
}
