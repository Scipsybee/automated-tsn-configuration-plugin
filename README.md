# Automated TSN configuration Plugin

This project intruduces a modular framework to automate offline scheduling in TSN networks to facilitate the design time and pre-simulation automated network configurations as well as interpretation of the simulations, based on [NeSTiNg TSN simulation Framework in OMNeT++](https://gitlab.com/ipvs/nesting). The automated TSN configuration framework is part of the project [DESTINE: Developing Predictable Vehicle Software Utilizing Time Sensitive Networking](http://www.es.mdh.se/projects/513-DESTINE) at the Mälardalen University.

This source persents a Plugin-in as Automated Configurator module of the framework, which is integrated in OMNeT++ environment. 

<img src="/img/softwareComponents.png" alt="Modules" width="100%">

Currently, you'll need to install plug-in development environment in OMNeT++ to run the code and use the automation plug-in tool. However, the executable file of the plug-in will be soon released for more convenient use of the tool.

## Getting Started

To use the plug-in tool, please follow the instructions in [OMNeT⁠+⁠+ IDE Deveper Guide](https://doc.omnetpp.org/omnetpp4/ide-developersguide/ide-developersguide.html#_installing_the_plug_in_development_environment) to install the IDE and start an existing project. 

The executable file of the configuration automation plug-in will be soon released for more convenient use of the tool.



## Project Documentations

B. Houtan, A. Bergström, M. Ashjaei, M. Daneshtalab, M. Sjödin, S. Mubeen, "An Automated Configuration Framework for TSN Networks", 22nd IEEE International Conference on Industrial Technology (ICIT'21), 2021

```
@inproceedings{Houtan6117,
	author = {Bahar Houtan and Albert Bergstr{\"o}m and Mohammad Ashjaei and Masoud Daneshtalab and Mikael Sj{\"o}din and Saad Mubeen},
	title = {An Automated Configuration Framework for TSN Networks},
	month = {March},
	year = {2021},
	booktitle = {22nd IEEE International Conference on Industrial Technology (ICIT'21)},
	url = {http://www.es.mdh.se/publications/6117-}
}
```
A. Bergström, Automatic Generation of Network Configuration in Simulated Time Sensitive Networking (TSN) Applications, Master Thesis, School of Innovation, Design and Engineering, Mälardalen University,Sweden, 2020.

```
@Misc{Master-Thesis-Albert-2020,
	note = {{A. Bergstr\"{o}m, Automatic Generation of Network Configuration in Simulated Time Sensitive Networking (TSN) Applications, Master Thesis, School of Innovation, Design and Engineering, M\"{a}lardalen University, Sweden, 2020}}
}
```
## Important

The Plugin is under continuous development: new features are being added, bugs are corrected, and so on. We will appreciate your comments sent to the address, [bahar.houtan@mdh.se](bahar.houtan@mdh.se).
